﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    public class YunJsonReturn
    {
        public int ErrCode { get; set; }
        public int RetCode { get; set; }
        public string ErrDesc { get; set; }
        public object RetData { get; set; }
    }
}
