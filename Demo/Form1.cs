﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SM2Test;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Caching;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;

namespace Demo
{
    public partial class Form1 : Form
    {
        string publicpubk = @"MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArfOzx+LjKF+BZKcbriPu
rWwmlrif+fQvZyqQq6hG8SWZRE52Ahp++3Fem79XdAu3U5jumvOeEKAfXMCClsfV
G9EqhLNSVA7Xb8zgnVelSHMPg9r2LX73nPSK28r3SoHAAuVNrva8f94koCYV8zym
I6W3duhDL/bfQDUkFS3MJcUb8bQcaxupKPLkxImBYGAjI3ceSMi984CFCcS8D6yU
tWGnxqy/nZVrfws7eI72FSpa2JaWkp7Bqm27bAMnirMx27rRN9uatHLjGBS60yrO
kZ1UJDkffi9tyOEIaEbNvUJWMH9rSAqiMpWH9Qdo9Vre4heMwNaxcFheYfty/o8Q
aQIDAQAB";

        string mFCARDWebAddr = "https://yun.pc15.net";
        public Form1()
        {
            InitializeComponent();
        }

        private void butCreateLoginciphertext_Click(object sender, EventArgs e)
        {
            string srcLoginciphertext = $"{txtCName.Text},{txtUserName.Text},{txtPassword.Text}";

            String plainText = srcLoginciphertext;
            // 公钥  
            // System.IO.File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory,"openssl_rsa_public_2048.pem"));
            publicpubk = publicpubk.Replace("\r\n", string.Empty);

            if (string.IsNullOrWhiteSpace(plainText) || string.IsNullOrWhiteSpace(publicpubk))
            {
                MessageBox.Show("请输入公钥和明文！");
                return;
            }

            RSACryptoService rsa = new RSACryptoService(string.Empty, publicpubk);
            string cipherText = rsa.Encrypt(plainText);
            txtLoginciphertext.Text = cipherText;
        }

        private void butAESEncryptor_Click(object sender, EventArgs e)
        {
            string srcLoginciphertext = $"{txtCName.Text},{txtUserName.Text},{txtPassword.Text}";
            txtEncryptor.Text = MyAES.Encryptor(srcLoginciphertext, txtKey.Text, txtIv.Text);
        }

        private void butAESDecryptor_Click(object sender, EventArgs e)
        {
            txtDecryptor.Text = MyAES.Decryptor(txtEncryptor.Text, txtKey.Text, txtIv.Text);
        }

        private void butLogin_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            ClientLogin(ref client);
            ObjectCache cache = MemoryCache.Default;
            var policy = new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddHours(1) };
            cache.Set("HttpClient", client, policy);
        }

        private void butGetInfo_Click(object sender, EventArgs e)
        {
            HttpClient client = GetHttpClient();
            string strParameters = "PeopleID=730";
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";//"application/x-www-form-urlencoded";
            client.Encoding = Encoding.UTF8;
            string strError = "";
            string strJson = client.PostData(mFCARDWebAddr + "/WebAPI/People/GetByID", strParameters, out strError);
            YunJsonReturn yunjsonreturn = JsonConvert.DeserializeObject<YunJsonReturn>(strJson);
            if (yunjsonreturn.RetCode == 1)
            {
                var oJobj = JsonConvert.DeserializeObject<JObject>(yunjsonreturn.RetData.ToString());
                txtEncryptor.Text = oJobj["PPassword"].ToString();
            }
        }
        private static object mLockobj = new object();
        public HttpClient GetHttpClient()
        {
            lock (mLockobj)
            {
                ObjectCache cache = MemoryCache.Default;
                var httpClient = cache["HttpClient"] as HttpClient;
                if (httpClient == null)
                {
                    httpClient = new HttpClient();
                    ClientLogin(ref httpClient);

                    var policy = new CacheItemPolicy() { AbsoluteExpiration = DateTime.Now.AddHours(1) };
                    cache.Set("HttpClient", httpClient, policy);
                }
              
                return httpClient;
            }
        }

        private void ClientLogin(ref HttpClient client)
        {
            client = new HttpClient();
            string strParameters = "Loginciphertext=" + HttpUtility.UrlEncode(txtLoginciphertext.Text) + "&JKeypublic=" + HttpUtility.UrlEncode(publicpubk) + "";
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";//"application/x-www-form-urlencoded";
            client.Encoding = Encoding.UTF8;
            string strError = "";
            string strJson = client.PostData(mFCARDWebAddr + "/WebAPI/Login/ClientLogin", strParameters, out strError);
            YunJsonReturn yunjsonreturn = JsonConvert.DeserializeObject<YunJsonReturn>(strJson);
            if (yunjsonreturn.RetCode == 1)
            {
                var oJobj = JsonConvert.DeserializeObject<JObject>(yunjsonreturn.RetData.ToString());
                txtKey.Text = oJobj["AKey"].ToString();
                txtIv.Text = oJobj["AIV"].ToString();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            HttpClient client = GetHttpClient();
            string strParameters = "DoorID=830&KeyboardUse_In=1&BodyTemperatureAlarmPar=350&OpenDoorTime=65535";
            client.Headers[HttpRequestHeader.ContentType] = "application/x-www-form-urlencoded";//"application/x-www-form-urlencoded";
            client.Encoding = Encoding.UTF8;
            string strError = "";
            string strJson = client.PostData("http://localhost:27087/WebAPI/Door/UpateDoor", strParameters, out strError);
            YunJsonReturn yunjsonreturn = JsonConvert.DeserializeObject<YunJsonReturn>(strJson);
            if (yunjsonreturn.RetCode == 1)
            {
            }
        }
    }
}
