﻿namespace Demo
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.txtCName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLoginciphertext = new System.Windows.Forms.TextBox();
            this.butCreateLoginciphertext = new System.Windows.Forms.Button();
            this.txtIv = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butLogin = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDecryptor = new System.Windows.Forms.TextBox();
            this.butGetInfo = new System.Windows.Forms.Button();
            this.butAESDecryptor = new System.Windows.Forms.Button();
            this.txtEncryptor = new System.Windows.Forms.TextBox();
            this.butAESEncryptor = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(578, 14);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.Size = new System.Drawing.Size(100, 21);
            this.txtPassword.TabIndex = 11;
            this.txtPassword.Text = "0000";
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(338, 14);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(100, 21);
            this.txtUserName.TabIndex = 10;
            this.txtUserName.Text = "system";
            // 
            // txtCName
            // 
            this.txtCName.Location = new System.Drawing.Point(58, 14);
            this.txtCName.Name = "txtCName";
            this.txtCName.Size = new System.Drawing.Size(100, 21);
            this.txtCName.TabIndex = 9;
            this.txtCName.Text = "xxxxx";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(531, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 8;
            this.label3.Text = "密码：";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(279, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 7;
            this.label2.Text = "操作员：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 6;
            this.label1.Text = "账套：";
            // 
            // txtLoginciphertext
            // 
            this.txtLoginciphertext.Location = new System.Drawing.Point(13, 86);
            this.txtLoginciphertext.Multiline = true;
            this.txtLoginciphertext.Name = "txtLoginciphertext";
            this.txtLoginciphertext.Size = new System.Drawing.Size(665, 43);
            this.txtLoginciphertext.TabIndex = 13;
            // 
            // butCreateLoginciphertext
            // 
            this.butCreateLoginciphertext.Location = new System.Drawing.Point(13, 56);
            this.butCreateLoginciphertext.Name = "butCreateLoginciphertext";
            this.butCreateLoginciphertext.Size = new System.Drawing.Size(145, 23);
            this.butCreateLoginciphertext.TabIndex = 12;
            this.butCreateLoginciphertext.Text = "生成Loginciphertext";
            this.butCreateLoginciphertext.UseVisualStyleBackColor = true;
            this.butCreateLoginciphertext.Click += new System.EventHandler(this.butCreateLoginciphertext_Click);
            // 
            // txtIv
            // 
            this.txtIv.Location = new System.Drawing.Point(421, 20);
            this.txtIv.Name = "txtIv";
            this.txtIv.Size = new System.Drawing.Size(256, 21);
            this.txtIv.TabIndex = 17;
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(57, 20);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(284, 21);
            this.txtKey.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(394, 23);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 15;
            this.label4.Text = "IV：";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(10, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(35, 12);
            this.label5.TabIndex = 14;
            this.label5.Text = "Key：";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butLogin);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtCName);
            this.groupBox1.Controls.Add(this.txtUserName);
            this.groupBox1.Controls.Add(this.txtLoginciphertext);
            this.groupBox1.Controls.Add(this.txtPassword);
            this.groupBox1.Controls.Add(this.butCreateLoginciphertext);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(709, 162);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RSA";
            // 
            // butLogin
            // 
            this.butLogin.Location = new System.Drawing.Point(184, 57);
            this.butLogin.Name = "butLogin";
            this.butLogin.Size = new System.Drawing.Size(75, 23);
            this.butLogin.TabIndex = 14;
            this.butLogin.Text = "登录";
            this.butLogin.UseVisualStyleBackColor = true;
            this.butLogin.Click += new System.EventHandler(this.butLogin_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.txtDecryptor);
            this.groupBox2.Controls.Add(this.butGetInfo);
            this.groupBox2.Controls.Add(this.butAESDecryptor);
            this.groupBox2.Controls.Add(this.txtEncryptor);
            this.groupBox2.Controls.Add(this.butAESEncryptor);
            this.groupBox2.Controls.Add(this.txtIv);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtKey);
            this.groupBox2.Location = new System.Drawing.Point(13, 181);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(708, 236);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "AES";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(11, 211);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 12);
            this.label9.TabIndex = 24;
            this.label9.Text = "人员密码：";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 183);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 12);
            this.label10.TabIndex = 23;
            this.label10.Text = "解密后内容：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(11, 121);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(65, 12);
            this.label8.TabIndex = 22;
            this.label8.Text = "人员密码：";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 95);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 12);
            this.label7.TabIndex = 21;
            this.label7.Text = "加密后内容：";
            // 
            // txtDecryptor
            // 
            this.txtDecryptor.Location = new System.Drawing.Point(93, 180);
            this.txtDecryptor.Multiline = true;
            this.txtDecryptor.Name = "txtDecryptor";
            this.txtDecryptor.Size = new System.Drawing.Size(584, 43);
            this.txtDecryptor.TabIndex = 20;
            // 
            // butGetInfo
            // 
            this.butGetInfo.Location = new System.Drawing.Point(127, 55);
            this.butGetInfo.Name = "butGetInfo";
            this.butGetInfo.Size = new System.Drawing.Size(86, 23);
            this.butGetInfo.TabIndex = 15;
            this.butGetInfo.Text = "获取用户信息";
            this.butGetInfo.UseVisualStyleBackColor = true;
            this.butGetInfo.Click += new System.EventHandler(this.butGetInfo_Click);
            // 
            // butAESDecryptor
            // 
            this.butAESDecryptor.Location = new System.Drawing.Point(12, 151);
            this.butAESDecryptor.Name = "butAESDecryptor";
            this.butAESDecryptor.Size = new System.Drawing.Size(75, 23);
            this.butAESDecryptor.TabIndex = 19;
            this.butAESDecryptor.Text = "解密";
            this.butAESDecryptor.UseVisualStyleBackColor = true;
            this.butAESDecryptor.Click += new System.EventHandler(this.butAESDecryptor_Click);
            // 
            // txtEncryptor
            // 
            this.txtEncryptor.Location = new System.Drawing.Point(93, 93);
            this.txtEncryptor.Multiline = true;
            this.txtEncryptor.Name = "txtEncryptor";
            this.txtEncryptor.Size = new System.Drawing.Size(584, 43);
            this.txtEncryptor.TabIndex = 14;
            // 
            // butAESEncryptor
            // 
            this.butAESEncryptor.Location = new System.Drawing.Point(12, 55);
            this.butAESEncryptor.Name = "butAESEncryptor";
            this.butAESEncryptor.Size = new System.Drawing.Size(75, 23);
            this.butAESEncryptor.TabIndex = 18;
            this.butAESEncryptor.Text = "加密";
            this.butAESEncryptor.UseVisualStyleBackColor = true;
            this.butAESEncryptor.Click += new System.EventHandler(this.butAESEncryptor_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 55);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(86, 23);
            this.button1.TabIndex = 25;
            this.button1.Text = "修改门禁属性";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 440);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.TextBox txtCName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtLoginciphertext;
        private System.Windows.Forms.Button butCreateLoginciphertext;
        private System.Windows.Forms.TextBox txtIv;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtDecryptor;
        private System.Windows.Forms.Button butAESDecryptor;
        private System.Windows.Forms.TextBox txtEncryptor;
        private System.Windows.Forms.Button butAESEncryptor;
        private System.Windows.Forms.Button butLogin;
        private System.Windows.Forms.Button butGetInfo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button1;
    }
}

